@def title = "Ecosystem participants"

# Integrated Services

- [**MapComplete**](https://mapcomplete.osm.be/) allows for leaving reviews of points of interest on [Open Street Map](https://www.openstreetmap.org/about).
- [**Open Camping Map**](https://opencampingmap.org/) redirects to [Mangrove Reviews](https://mangrove.reviews/) and allows reading of campsite reviews.

## Regional tourist boards

- [**Toggenburg Tourismus**](https://toggenburg.swiss/)
- [**Thurgau Tourismus**](https://thurgau-bodensee.ch/)
- [**Heidiland Tourism**](https://heidiland.com/)
- [**House of Winterthur**](https://winterthur.com/)
- [**Ostschweiz Tourismus**](https://ostschweiz.ch/)
- [**Haslital Tourismus**](https://haslital.swiss/)

# Corporate Members

- \fig{./corporate-IFITT.png}
- \fig{./corporate-STI2.png}
- \fig{./corporate-HSR.png}
