@def title = "Technology"

# Technology

The open-source infrastructure we developed is called **Mangrove**, it allows us to create and maintain the dataset of open reviews, for everyone to be able to use it freely.

All implemented components are available under Apache License 2.0 and you can inspect or contribute to their [code](https://gitlab.com/open-reviews).

## How can I integrate Open Reviews into my website or app?

* Interact directly with the [**REST OpenAPI**](https://docs.mangrove.reviews/), which allows for easy submission and retrieval of reviews from the open dataset.
* Interact via **client-side [JS](https://js.mangrove.reviews/) or [TypeScript](https://www.npmjs.com/package/mangrove-reviews-typescript) library** which has the same capabilities as the API and assists in management of reviewer identity.
* Make use of our [**web widget**](/technology/widget) by pasting in an HTML snippet on your site. It will allow your users to read and write Open Reviews, while the customisation tools give you control over what and how things are displayed.

[**Mangrove Demo**](https://mangrove.reviews) showcases how one can build an app around the JS library. It demonstrates the capabilities of the infrastructure, allowing to read and write reviews of places (points of interest), legal entities, websites, books, and more.

## How does it work?

* [**Technical standard including the data format**](https://mangrove.reviews/standard) to ensure interoperability and allow for reviews to be analyzed programmatically.
* [**Implementations**](https://gitlab.com/open-reviews/mangrove) including the server and all components which rely on it.
* Open-source [**probabilistic aggregation algorithm**](https://gitlab.com/open-reviews/mangrove/-/tree/master/aggregator) to ensure reliability of the data.
* Every review is stored in an Open Dataset licensed under CC-BY-4.0. You can [**download**](https://api.mangrove.reviews/reviews) and make use of the dataset freely, as long as you agree to protect user privacy and copyright laws.

## Principles

For the Open Reviews Ecosystem to succeed and the infrastructure to be maintainable by the community, we believe the underlying technology has to ensure three main qualities:
* The data format has to be **useful** for as many parties as possible, so that the dataset is valuable and attractive to use. 
    * Capture as many aspects as necessary for different use cases
    * Open and free-of-charge access 
* The infrastructure needs to be **easy to integrate** into websites and apps for reading and writing reviews, so that the ecosystem gains integrators and the dataset grows.
    * Clear technical standard that ensures interoperability
    * Easy-to-use and well-documented APIs
    * Clear legal framework to provide confidence in the integration
* The infrastructure needs to be **easy to maintain** at low cost and little overhead.
    * Reliable and persistent database server
    * Open source development to allow for redundancy and open innovation
    * Effective governance structures to ensure the infrastructure can evolve as the needs of the ecosystem participants change
