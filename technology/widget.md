# Open Reviews Widget

The Open Reviews web widget is a web application that can be embedded into any website or application to allow users to read and write Open Reviews. It allows you to integrate review functionality into your website with just a few clicks, by adding html snippets such as this one. 

```html
<link rel="stylesheet" type="text/css" href="https://widget.mangrove.reviews/or-widget.css"/>
<div class="or-review" data-sub="https://example.com" data-title="https://example.com"></div>
<script type="text/javascript" src="https://widget.mangrove.reviews/or-widget.js"></script>
```
Find below numerous ways to customise your widget to be in full control of what is shown on your website or app.

IMPORTANT NOTE: Please read our [Disclaimer](#disclaimer) and our Privacy Policy carefully before integrating the Open Reviews Widget on your own website.

## Setup and configuration

Add a link to the css file in your `<head>`:
```html
<!-- Add the or-widget.css with default styling -->
<link rel="stylesheet" type="text/css" href="https://widget.mangrove.reviews/or-widget.css"/>
```

Before your closing `<body>` tag add:

```html
<script type="text/javascript" src="https://widget.mangrove.reviews/or-widget.js"></script>
```

Then you can add the Open Reviews widget anywhere on your page by adding a `div` with `class="or-review"` and our custom `data` attributes (described in the next section).

Example:

```html
<div class="or-review" data-sub="https://example.com" data-title="https://example.com"></div>
```

## Settings

Following parameters can be set through `data-*` attributes:

| attribute          | type    | required | description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ------------------ | ------- | -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `sub`              | string  | Yes      | This attribute is used to specify the **Subject**, which is a unique string that identifies something that is being reviewed (e.g., a point of interest). For more information refer to these [Definitions](https://mangrove.reviews/standard#definitions). Refer to the section [Obtaining the Subject Identifier](#obtaining_the_subject_identifier) below to see how to obtain this subject.                                                                                                                                                                       |
| `title`            | string  | Yes      | This is the title displayed in the review form when a user wants to add a new review                                                                                                                                                                                                                                                                                                                                                                                                            |
| `blacklist`        | string  | No       | This attribute can be used to block certain reviews from appearing on the widget. You need to provide a comma-seperated list of strings with the signatures of all the reviews that should be blocked. Refer to the section [Obtaining the Review Signature](#obtaining_the_review_signature) below to see how to obtain them. Example:    `-yMnp3TZIHcP5c-tY2G5tds1cmn2vcOJmHAAXbwFDG6m4ZZnoMgfsKj6a1uS3cFLPWR4ZgGX0g6lkGQYwx4xOg, jKbNkaKLsLlvQflcMZRY5HSbhmVIBywW7oGWQd1LvsfmBUZMq3eA9Pn8_Lrqg3lski8fdCY9hVChEKYN60lOUA`      |
| `hide-photos`      | boolean | No       | Hide uploaded photos from the reviews. Defaults to `False`                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| `filter-opinion`   | boolean | No       | Hide reviews without an opinion text. Defaults to `False`                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `filter-anonymous` | boolean | No       | Hide reviews without a name (anonymous reviews). Defaults to `False`                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `rating-algorithm` | string  | No       | Defines the method used to calculate the overall rating. Possible values are - `mangrove`, `local`. `mangrove` will determine the rating with the help of ORA's open-source probabilistic aggregation algorithm that assigns a reliability to a reviewer based on several factors. `local` will calculate a simple average of those reviews that you choose to display on your website (this might be relevant if you make extensive use of the `blacklist` attribute).  Defaults to `mangrove` |
| `language`         | string  | No       | This can be one of the following languages - `en, fr, pl, de, pt` which will make the widget appear in the selected language, or `selector` which will display a selector for the users to choose between different languages. It only affects the language of the buttons and tags, but not of the reviews themselves (it is not a translation tool). If no value is provided, then the language will be picked up dynamically from the user's locales, with english as a fallback.            |
| `reviews-per-page` | integer | No       | Number of reviews shown on each page. Defaults to `20`                                                                                                                                                                                                                                                                                                                                                                                                                                          |
## Obtaining the Subject Identifier
You can obtain the `sub` attribute to use from the [Mangrove Reviews](https://mangrove.reviews/) website. Search for your resource, click the menu icon shown below and click `Copy subject identifier`

![Obtaining the subject identifier](/assets/technology/sub_demo.gif)

## Obtaining the Review Signature
To obtain the signature of a review that you want to blacklist, you can click on the menu button on the bottom right of the review card and use the **Copy review signature** option, as shown below.

![Obtaining the Review Signature](/assets/technology/copy_signature.gif)


## Custom styling

All widget elements have class names with `or-` prefix. To make any changes to the looks of the widget, you can override the css for these classes. 

You can use the dev tools in your browser to identify which class to modify. In Google Chrome, this can be done by simply right clicking on any part of the widget and clicking inspect element.

For instance, here's the css you can use if you want to change the background of the review cards, and the fill and text color of the rate and review button

```css
.or-review-wrapper {
  background-color: cyan;
}

.or-review-subject-rate-button button{
  background-color: lime;
  color: black;
}
```


## Example implementation of embedded widget
Here you can see an example implementation of the Open Reviews widget within this website. It is connected to the subject "https://example.com" that we use for testing and showcasing. 

Feel free to enter a test review here, to comment on another review, or to use the different interaction buttons, and to experiment with the cryptographic keys for maintaining your identity across different devices if you like.

When you integrate the widget into your own website, you will choose on which page and under which subject the reviews functionality will appear. 

***


~~~
<h1><p style="text-align: center;">EXAMPLE</p></h1>
~~~


~~~
<link rel="stylesheet" type="text/css" href="https://widget.mangrove.reviews/or-widget.css"/>
<script type="text/javascript" src="https://widget.mangrove.reviews/or-widget.js"></script>
<div class="or-review" data-sub="https://example.com" data-title="https://example.com" data-language="selector" data-reviews-per-page=5></div>
~~~

***

## DISCLAIMER

By integrating the Open Reviews Widget on your website, you understand and accept that the Open Reviews Widget is provided “as is” and without warranties or representations of any kind either expressed or implied. Any use, reproduction, and distribution of the Open Reviews Widget shall be governed by the Apache License version 2.0. 

All users reviewing and putting content (“Reviewers”) in the Open Review Dataset need to accept our [general terms and conditions](https://mangrove.reviews/terms) (“Open Reviews General Terms and Conditions”) and confirm that any content uploaded does not violate any law. While the Open Reviews Association provides mechanisms that aim to ensure a high reliability of ratings (Probabilistic Aggregation Algorithm, possibility for other users to flag content, possibility for users and business owners to comment on reviews, etc.) Open Reviews Association does not monitor the content actively and cannot guarantee that any content of the Open Reviews Dataset is in compliance with Open Reviews General Terms and Conditions and any applicable law. You acknowledge and accept that if you choose to implement the Open Reviews Widget in your website, that you control the content and that you are solely responsible for any content displayed via the Open Reviews Widget on your website. The content to be displayed on Integrator's website can be controlled by the Integrator via the attributes described in the settings section of the Open Reviews Widget.

*Limitation of Liability*: To the maximum extent permitted by Applicable Law, in no event shall Open Reviews Association be liable to you for damages, including any direct, indirect, special, incidental, or consequential damages of any character arising as a result of the use of the Open Reviews Widget and/or any displayed content on the Open Reviews Widget. 

*Indemnification*: You shall indemnify and hold Open Reviews Association and is affiliates, offices, directors, employees, agents, shareholders, licensors, licensees, assigns or successors harmless from any claim or demand (including but not limited to reasonable attorney fees and costs of investigation) made by a third party due to      or arising out of or related to your use of the Open Reviews Widget or your violation of any laws, regulations, or third party rights.

Any dispute in connection with Integrators’ use of the Open Reviews Widget shall be submitted to the exclusive jurisdiction of the Courts of Zug, Switzerland. The use of the Open Reviews Widget by Integrator shall be governed by and construed and interpreted in accordance with the substantive laws of Switzerland, excluding the Swiss conflict of law rules. The United Nations Convention for the International Sales of Goods ("Vienna Sales Convention") is excluded.
