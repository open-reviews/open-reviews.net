# isPersonalExperience

True if if the review has been left by a reviewer who had direct experience with the subject of the review and is not based on a third party account.