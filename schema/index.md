# Open Reviews RDF Schema

[Explore the Domain Specification](https://semantify.it/domainspecifications/public/4Ztib7sjX) or see the [OpenReview schema as JSON-LD](OpenReview.jsonld).