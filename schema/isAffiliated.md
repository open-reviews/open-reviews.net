# isAffiliated

True if the review has been left by a reviewer affiliated with the subject, such as business owner or employee.
