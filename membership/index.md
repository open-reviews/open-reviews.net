@def title = "Join Us"

# Become a Member

Members are vital to this movement and we welcome everyone who supports our goals to join us. The biggest benefit provided by the Open Reviews Association is being part of a strong community and network that supports the discussion, exchange and development of knowledge about the use and impact of Open Data Reviews. 

Why should you join?

* Influence the future direction of the project. Vote in the annual ORA elections for the board that steers the project and organisation.
* Participate in working groups and support the movement actively by contributing your skills and interests, whether that is promoting and connecting ORA to relevant parties, helping with comms and social media, improving the code base, giving a hand to integrators, coordinating research activities, or fundraising: any contribution is welcome!
* Work with Corporate Members to establish industry-wide standards and tools to achieve a fast adoption of Open Reviews by apps, websites, and end users.
* Receive updates on issues, project collaboration opportunities, events and resources.
* Network and meet other members all over the world.

We have two membership types: **Individual**, and **Corporate**. 
* Individual membership is available to all natural persons. The membership enables you to influence the direction of ORA and the Mangrove infrastructure by being able to vote in elections for officers of the association. 
* Corporate members can have their logo, company profile, and their contribution to the ecosystem included on the website. All delegates may attend ORA General Assembly, but Corporate members have only one vote per organisation. 

All membership types are free-of-charge.

~~~
<a href="https://forms.gle/uCweTQA1ef2pQBam8" target="_blank">Sign up for <b>membership</b> here</a>.
~~~

[Read our **Articles of Association**](https://open-reviews.net/membership/articles-of-association).
