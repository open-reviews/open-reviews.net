@def title = "Articles of Association"

# Articles of Association

\toc

## NAME AND DOMICILE
 
### Article 1
 

The “Open Reviews Association" – referred to hereafter as the “Association” or “ORA” – is a not-for-profit association (Verein) and is a legal entity according to articles 60 ff. Swiss Civil Code (“CC”).

Its duration is unlimited.

 
### Article 2
 

The Association is domiciled in the City of Zug, Switzerland.

 

## PURPOSE AND ACTIVITIES
 
### Article 3
 
**Purpose**
 
The Open Reviews Association (ORA) is an international not-for-profit organization established to build a global Open Data Ecosystem for online customer reviews. To achieve this goal, it supports the adoption, maintenance, and growth of Mangrove: a free and open-source **software infrastructure** for reading/writing reviews in an **open dataset** that is freely accessible to anyone.

The association fulfills the following key functions:

* Act as the legal entity for the Mangrove technology, and the custodian for the computer servers and services necessary to host the open dataset.
* Support and communicate with the working groups, with the association board at times delegating various tasks to the working groups.
* Provide a vehicle for fund-raising to support the vision of the project.

The association invites industry practitioners, researchers, entrepreneurs, developers, open data enthusiasts, and generally anyone interested in open reviews to join as members and support its cause.

 
### Article 4
 
**Activities**
 

The ORA may conduct and promote all business and/or enter into all transactions and generally perform all acts as may be necessary, appropriate, incidental or desirable to assist in achieving or furthering its purpose.

 

 

In particular the Association shall:

 

1. represent the interests of its Members, in line with the Association’s purpose, vis-à- vis national and international authorities and organizations;

 

2. have the capacity to apply for admission as a representative into any national or international organization or ventures in the fields relevant to its purpose;

 

3. promote the present and future possibilities of applications, businesses and initiatives in the fields of open data customer reviews and related technologies;

 

4. promote and develop best practices, standards and market self-regulations which effectively prevent abuse and fraud, with the aim of contributing to a sustainable development of the ecosystem beneficial to the society as a whole;

 

5. cooperate with national,regional or international organizations that are of special interest to its Members, in line with the Association’s purpose;

 

6. support the Members to identify with and to contribute to the development of the ecosystem, as well as to contribute to the network of the Association, including, but not limited to the establishment of regional and international branches and chapters of the Association;

 

7. organize events (notably scientific and educational events), foster and promote scientific research and academia in the related fields by partnering with public and private institutions for the benefit of its Members; provide educational and other services, free or against payment, organize meetings of the Members and facilitate contacts between Members;

 

8. investigate and disseminate information regarding industry challenges and opportunities, as well as emerging practices and technology including promoting of research papers and thought leadership of its Members.

 

Within the limits of this article the Association may borrow funds, provide guarantees and may develop any appropriate sources of funding.

 

## MEMBERSHIP
 

### Article 5
 

**Members**
 

Members of the Association (Members) can be natural persons and legal entities, clearly identified, which acknowledge and support the Association’s purpose.

 

The ORA has the following categories of Members:

 

1. Individual Members;

2. Corporate Members

The Board may define further (sub)categories of Members.

 

 

### Article 6
 
**ORA Officers**
 

Members holding an office or position in the Association as set out by the Board shall qualify as ORA Officers.

 

The Board ascertains that ORA Officers identify with, adhere to and contribute to the mission and purpose of the Association. ORA Officers discharge their duties at all times in an ethical manner and in full compliance with all applicable laws, rules and regulations. 

 

Appointments may be revoked anytime by the body which made the nomination/election if the ORA Officer does not carry out the responsibilities assigned to him or her.

 

 
### Article 7
 

**Admission procedure**
 

Membership is granted by the Board only following acceptance of a formal application addressed to the Secretariat via the ORA website.


 
### Article 8
 
**Membership fees and contributions**
 

The acting Board has not set a Membership fee yet, but might do so after the General Assembly in the year following the Association's foundation. In case a Membership fee is decided, each Member will have to pay an annual fee. Membership fees will be due on demand for payment and shall be paid within 30 days.

The Association shall be under no obligation to refund any fees or contributions of the Member already paid.

 

### Article 9
 
**Loss of membership**
 

Membership terminates by:

 

1. Resignation in writing by a Member, addressed to admin@open-reviews.net;

2. Exclusion of a Member in accordance with article 10;

3. Death or insolvency of a Member;

4. Dissolution of the Association.

 

Termination of membership shall be effective immediate.

 

### Article 10
 
**Sanctions**
 

The Board – or in case of Board members an ad-hoc committee – may impose the following sanctions upon Members, including Board members and other ORA Officers, who do not comply with material obligations under the Articles of Association.


1. Reprimand;
2. Contractual penalty;
3. Exclusion.
 
A sanction may only be imposed based on a written application by the Secretariat detailing the alleged acts of non-compliance, explaining why these acts are material and after the Member had an opportunity to be heard by the Board. Any sanction must be proportionate.

 

The reprimand shall be issued in writing and state that additional sanctions, including an exclusion, will be imposed if the Member does not fully comply with his or her material obligations under the ORA Articles of Association.

 

The Board may impose contractual penalties of up to five times the annual membership fee to be paid by that Member.

 

A Member may be excluded by resolution of the Board adopted with a two-thirds majority of all Board members.

Any Board resolution imposing sanctions shall be final.

 

The exclusion is effective immediately.

 

 

 

## ORGANIZATION
 

**A. Official Bodies and Divisions of the Association**
 
### Article 11
 

**The bodies of the Association are:**

The General Assembly;
The Board of Directors (the Board);
The Secretariat;
The Auditors (optional).
 

 

**B.  General Assembly**
 
### Article 12
 
**Constitution, Agenda, Participation**
 
The General Assembly is composed of all Members and has the power to decide on fundamental questions and issues of the Association.

 

The Chairperson shall chair its meetings. The General Assembly meets validly regardless of the number of Members present in person or remotely.

 

The General Assembly will take place annually within the first six months of the year in regular sessions, and can also meet in extraordinary sessions whenever necessary, as determined by the Board.

 

The Board will give notice to the Members by electronic communication or online publication, setting out the agenda items proposed by the Board no later than 15 days prior to the date of the General Assembly.

 

Motions for consideration must be directed to the Secretariat, at the latest four weeks prior to the date of the General Assembly. The Board is obliged to include the motions in the agenda.

 

The General Assembly shall be organized in such a manner that Members may participate either in person or remotely. The Association may implement an electronic or online voting system.

 
 

### Article 13
 
**Extraordinary General Assembly**
 

An extraordinary General Assembly may be convened based on a resolution of the Board, a motion of the Auditors or if at least ten Members demand it by written request outlining the purpose and reasons for the meeting to the Board.

 

Additional motions for consideration must be directed to the Secretariat, at the latest four weeks prior to the extraordinary General Assembly. The Board is obliged to include the motions in the agenda and to distribute the updated agenda to all Members no later than one week prior to the extraordinary General Assembly.

 

 
### Article 14
 

**The General Assembly shall have the following powers:**

 

1. Approve the annual report, the annual financial statements, and the report of the Auditors (if applicable)

 

2. Resolutions discharging the Board, and the Auditors (if applicable);

 

3. Determination of the membership fees;

 

4. Determination of the annual budget;

 

5. Election of the Board members, and the Auditors;

 

6. Handle motions of the Board and the Members;

 

7. Amend the Articles of Association; and

 

8. Dissolve the Association.

 

 

### Article 15
 

The General Assembly can discuss and decide only those matters mentioned in the agenda sent with the notice of the meeting.

 

Resolutions of the General Assembly shall be passed by the majority of the Members participating in the vote. Resolutions relating to amendments of the Articles of Association or the dissolution of the Association shall be passed if supported by two thirds of the votes cast.

 

The proceedings of the General Assembly are to be recorded in minutes to be signed by the Chairperson and the scrutineer of the meeting. Any extracts made from such minutes must be signed by the scrutineer and one member of the Board.

 

All Members have one vote.

 

A Member who is involved in a transaction or litigation with the Association or whose spouse or whose directly related relative is involved in such a transaction or litigation has no right to vote on any issue relating to the transaction or the litigation. Only Members with a valid membership of more than one month may vote at that General Assembly.

 
 

**C.  The Board of Directors**
 
### Article 16
 

The Board will be composed of a minimum of two and a maximum of five members and will be elected by the General Assembly.

 
The Board has a quorum when at least 50% of the members and a minimum of 2 members are present. Upon motion of the Chairperson or on request of a member of the Board, the Board will convene.

 
The Board may establish a separate committee assisting the preparation procedures for elections to the Board (Nomination Committee).


 

 

### Article 17
 
**Composition, terms of office**
 

The Board will be composed of:

 

1. Chairperson

2. Secretary (Actuary)

4. Treasurer

5. Additional members

 

The members of the Board are elected by the General Assembly for a term of office which ends with the completion of the next ordinary General Assembly.

 

Members whose term of office has expired are immediately eligible for re-election. The Board may propose a collective re-election to the General Assembly.

 

The Board shall constitute itself. It shall elect, based on the principles of seniority and rotation, a Chairperson.

 

The Chairperson, as appointed by the Board, shall be confirmed by the General Assembly. Accumulation of functions is acceptable, however the Chairperson shall not at the same time serve as Secretary or Treasurer.

 

Where a vacancy, including the office of the Chairperson, arises by reason of death, incapacity or immediate resignation of a Board member, the Board may appoint an interim substitute until the next extraordinary or ordinary General Assembly.

 

 

 

### Article 18
 

The Board generally has all powers and duties that are not expressly assigned to the General Assembly or to any other specific body such as the Secretariat.

 

The Board has the mission of defining the strategy of the Association, its plans for development as well as promoting the Association and growing the ecosystem. In this regard, the Board reaches out to the Members for feedback on a regular basis.

 

In particular, the Board’s powers and duties include the following:

 

1. Preparation and execution of ordinary and extraordinary General Assemblies;

 

2. Proposal of amendment of the Articles of Association;

 

3. Admission and exclusion of Members;

 

4. Adoption of Organizational Regulations including the organization of the Association and the remuneration of ORA Officers, of employees and independent contractors and any additional compensation for particularly time-consuming work performed by individual Members;

 

5. Establishment of special committees; the Board defines the duties of such bodies and determines the details of management, including remuneration (if any);

 

6. Establishment of the Association’s working groups (the Working Groups); the Board defines the duties of the Working Groups and determines the details of management, including remuneration (if any).

 

The Board strives to ensure transparency, inclusivity, diversity and for equal opportunities in and among the bodies of the ORA. The Board reports annually to the General Assembly on this matter.


 

### Article 19
 
The Board and the Chairperson publicly represent the Association. Members of the Board are to avoid any form of conflict of interest.

 

The Chairperson, the Secretary and the Treasurer shall have a right to bind the Association when signing jointly by two. Any transaction with third parties, including payment transfers shall require authorization jointly by two.

 
**D. The Secretariat and Executive Director**
 
### Article 20
 
The Board appoints a qualified professional (the Executive Director) to lead and manage the Secretariat and shall determine his or her terms of engagement. The Executive Director can appoint other employees of the Secretariat.

 

The Secretariat carries out the decisions of the General Assembly and of the Board and ensures the effective day-to-day functioning of the Association.

 

The Secretariat is the central internal and external liaison office for administrative and operational matters of the Association. The Secretariat shall have primary responsibility for the exchange of information between the Board, the committees, the Working Groups and any other additional bodies, thereby maintaining transparency of key activities, according to the Organizational Regulations.

 

In particular, the Secretariat:

 

a) may appoint persons to be in charge of specific projects and specify the duration of such projects;

 

b) draws up the list of committees and Working Groups and manages the overall project portfolio of the Association;

 

c) may create task forces on an ad-hoc basis to deal with specific problems or questions, and appoints their chairpersons.

 

The Secretariat shall have primary responsibility for the internal communication, including the reporting to the General Assembly and the Members.

 

 
**F. Auditors**
 
### Article 21
 

The Auditors are elected by the General Assembly and can be recruited from Member volunteers as long as the size of the Association does not meet criteria for professional auditing as per Swiss Code of Obligations. A Member may request the election of Auditors for a limited audit of the accounting even if the above criteria are not met.

 

 

### Article 22
 

The Auditors can be comprised of one or more natural persons or legal entities or partnerships. The Auditors must be certified and independent pursuant to the Swiss legal framework.

 

The Auditors have to have their residence, domicile or registered branch in Switzerland. If the Association has multiple Auditors at least one of them needs to fulfil the above criteria.

 

If the Association is obligated to perform a regular statutory audit, the General Assembly needs to elect an authorized expert auditor respectively a regulated audit company according to the laws of the Swiss Audit Control Law (Revisionsaufsichtsgesetz) of December 16, 2005.

 

If the Association is obligated to perform a limited statutory audit, the General Assembly needs to elect a controller according to the laws of the Swiss Audit Control Law (Revisionsaufsichtsgesetz) of December 16, 2005.

 

The Auditors will be elected for one year. Their mandate terminates with the approval of the annual financial statements. A re-election is possible. A dismissal is possible any time and without notice.

 

 

### Article 23
 

The fiscal year shall coincide with the calendar year. Financial statements shall be completed and an inventory taken for the fiscal year ending December 31st.

 
##  NET EARNINGS AND ASSETS OF THE ASSOCIATION
 
### Article 24
 
**Resources**
 

The assets of the Association are made up of membership fees and any subscriptions for services, penalties paid by the Members, public or private grants or subsidies, any gifts, contributions, sponsoring, event fees, legacies, balance sheet surpluses and all other sources permitted by law.

 

The Association is organized and shall be administered and operated to receive, administer, and expend funds to permit and represent the common business interests of and improve business conditions among and for its Members.

 

No part of the assets or net earnings of the Association shall inure to the personal benefit of or be distributable to its Members, past and current bodies or to others. The Association, however, shall be fully authorized and empow- ered to employ staff and pay reasonable compensation / salaries for them including adequate reimbursement for Board members or any other bodies or Members rendering valuable services for the purposes of the Association.

 

Only the Association’s assets shall cover for the liabilities of the Association. The Members’ personal liability for the liabilities of the Association is excluded.

 
## AMENDMENT OF THE ARTICLES OF ASSOCIATION AND DISSOLUTION
 

### Article 25
 

Any 10 Members acting jointly may propose, in writing to the Secretariat, with a copy to the Chairperson, that the Articles of Association be amended by a special resolution. The Board shall ensure that the proposal is distributed among all Members and is included as an agenda item in the business of the next General Assembly.

 

The amendment of Articles of Association requires at least two-thirds of the votes cast.

 

In the event of the dissolution of the Association, the General Assembly determines the distribution of the liquidation proceeds.

 

Upon dissolution of the Association, all of its assets and property of every nature and description remaining after the payment of all liabilities and obligations of the Association shall be paid over and transferred to one or more not-for-profit organizations which engage in activities substantially similar to those of the Association.

 

In no event shall any of such assets or property be distributed to any Member – including members of the Board and other ORA Officers – members of the Secretariat or any other members of the Association’s administration or to any third party private individual.

 

##  LANGUAGE

### Article 26
The official language and working language of the ORA shall be English.

Any references to functions in these Articles of Association shall apply to all genders.

These Articles of Association were approved as version 1.0 at the General Assembly of 13 July 2020.

