@def title = "Working Groups"

# Working Groups

After the initial [building blocks of infrastructure](https://open-reviews.net/technology) have been developed, we can now bring all interested parties together and work jointly towards making reviews open for the world. We defined the following 4 working groups as a starting point:

### Development
We maintain and further develop the infrastructure required for the Open Data Ecosystem for Reviews to function. Furthermore, we provide support to parties interested in integrating open reviews infrastructure into their application or website.

### Research
We maintain ties with academic institutions to drive research initiatives within the different areas of the open reviews technology as well as into use cases for reviews data.

### Adoption
We create and execute communication strategies to make the public aware of the open reviews movement and the open-source technology available for integration and free usage. We reach out to individuals and organisations to explore together how they could benefit from becoming part of the ecosystem. Furthermore, we participate in grant applications and coordinate fund-raising initiatives to secure the financial means that will help to drive the adoption of the technology and the growth of the ecosystem.

### Administration
We take care of our members and help them to get the most out of their membership; furthermore, we administer the finances and legal matters of the association.


> If you are interested in joining any of the groups please drop us an email.
